import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'

import socketIOClient from 'socket.io-client'
import sailsIOClient from 'sails.io.js'
import vueSails from 'vue-sails'

const io = sailsIOClient(socketIOClient)
// Additional Sails.io.js configuration.
io.sails.url = process.env.VUE_APP_API_URL || 'http://0.0.0.0:1338'
io.sails.environment = process.env.NODE_ENV || 'development'
Vue.use(vueSails, io)

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
