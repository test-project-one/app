export default {
  methods: {
    async apiData (method, url = '', data = {}) {
      return new Promise((resolve, reject) => {
        this.$io.socket[method](`/${url}`, data, (body, jwres) => {
          if (jwres.statusCode !== 200) {
            return reject(jwres)
          }
          resolve(body)
        })
      })
    }
  }
}
